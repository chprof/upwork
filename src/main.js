import './assets/styles/style.scss'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuelidate from 'vuelidate'
import VueLazyload from 'vue-lazyload'
import VModal from 'vue-js-modal'
import Scrollactive from 'vue-scrollactive'
import VueCollapse from 'vue2-collapse'

Vue.use(VModal)
Vue.use(Vuelidate)
Vue.use(Scrollactive);
Vue.use(VueCollapse)
Vue.use(VueLazyload, {
	preLoad: 1.3,
	error: require('./assets/images/page-not-found.svg'),
	loading: require('./assets/images/eclipse.svg'),
	attempt: 1,
	lazyComponent: true
})

import ClickOutside from './directives/click-outside.js'
Vue.directive('click-outside', ClickOutside)

Vue.config.productionTip = false

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')
