import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
	{
		path: '/',
		name: 'Home',
		component: Home,
		meta: {
			frontMainTitle: true,
			frontSearchVacancyForm: true,
			headerBackgroundDefault: true
		},
		children: [
			{
				path: 'about-us',
				name: 'about-us',
				component: () => import('../views/About-us.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				}
			},
			{
				path: 'post-vacancy',
				name: 'post-vacancy',
				component: () => import('../views/Post-vacancy.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				},
			},
			{
				path: 'post-resume',
				name: 'post-resume',
				component: () => import('../views/Post-resume.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				},
			},
			{
				path: 'vacancy-category',
				name: 'vacancy-category',
				component: () => import('../views/Vacancy-category.vue'),
				meta: {
					simpleHeader: true
				}
			},
			{
				path: 'vacancy-category-inside',
				name: 'vacancy-category-inside',
				component: () => import('../views/Vacancy-category-inside.vue'),
				meta: {
					frontSearchVacancyForm: true,
					headerSeachSmall: true,
					headerBackgroundDefault: true
				}
			},
			{
				path: 'vacancy-page',
				name: 'vacancy-page',
				component: () => import('../views/Vacancy-page.vue'),
				meta: {
					innerPage: true,
					headerBackgroundDefault: true
				}
			},
			{
				path: 'vacancy-city',
				name: 'vacancy-city',
				component: () => import('../views/Vacancy-city.vue'),
				meta: {
					simpleHeader: true
				}
			},
			{
				path: 'vacancys-by-company',
				name: 'vacancys-by-company',
				component: () => import('../views/vacancys-by-company.vue'),
				meta: {
					simpleHeader: true
				}
			},
			{
				path: 'employers-speciality',
				name: 'employers-speciality',
				component: () => import('../views/Employers-speciality.vue'),
				meta: {
					frontMainTitle: true,
					frontSearchVacancyForm: true,
					headerBackgroundDark: true
				}
			},
			{
				path: 'resume-category-inside',
				name: 'resume-category-inside',
				component: () => import('../views/Resume-category-inside.vue'),
				meta: {
					frontSearchVacancyForm: true,
					headerSeachSmall: true,
					headerBackgroundDark: true,
				}
			},
			{
				path: 'resume-page',
				name: 'resume-page',
				component: () => import('../views/Resume-page.vue'),
				meta: {
					innerPage: true,
					headerBackgroundDark: true
				}
			},
			{
				path: 'company-page',
				name: 'company-page',
				component: () => import('../views/Company-page.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				}
			},
			{
				path: 'user-page',
				name: 'user-page',
				component: () => import('../views/User-page.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				}
			},
			{
				path: 'news-page',
				name: 'news-page',
				component: () => import('../views/News-page.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				}
			},
			{
				path: 'news-page-tags',
				name: 'news-page-tags',
				component: () => import('../views/News-page-tags.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				}
			},
			{
				path: 'news-inside-page',
				name: 'news-inside-page',
				component: () => import('../views/News-inside-page.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				}
			},
			{
				path: 'contact-page',
				name: 'contact-page',
				component: () => import('../views/Contact-page.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				}
			},
			{
				path: 'help-page',
				name: 'help-page',
				component: () => import('../views/Help-page.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				}
			},
			{
				path: 'company-card',
				name: 'company-card',
				component: () => import('../views/Company-card.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				},
				children: [
					{
						path: 'company-vacancys-feedback',
						name: 'company-vacancys-feedback',
						component: () => import('../views/Company-vacancys-feedback.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
						children: [
							{
								path: 'company-feedback',
								name: 'company-feedback',
								component: () => import('../views/Company-feedback.vue'),
								meta: {
									innerPage: true,
									headerBackgroundImgPersonal: true
								},
							}
						]
					},
					{
						path: 'company-event-log',
						name: 'company-event-log',
						component: () => import('../views/Company-event-log.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
					{
						path: 'company-services-accounts',
						name: 'company-services-accounts',
						component: () => import('../views/Company-services-accounts.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
					{
						path: 'company-mailing-list',
						name: 'company-mailing-list',
						component: () => import('../views/Company-mailing-list.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
					{
						path: 'company-settings',
						name: 'company-settings',
						component: () => import('../views/Company-settings.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
					{
						path: 'company-account-settings',
						name: 'company-account-settings',
						component: () => import('../views/Company-account-settings.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
					{
						path: 'company-users',
						name: 'company-users',
						component: () => import('../views/Company-users.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					}
				]
			},
			{
				path: 'personal-card',
				name: 'personal-card',
				component: () => import('../views/Personal-card.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				},
				children: [
					{
						path: 'personal-resumes',
						name: 'personal-resumes',
						component: () => import('../views/Personal-resumes.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						}
					},
					{
						path: 'personal-response-history',
						name: 'personal-response-history',
						component: () => import('../views/Personal-response-history.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
					{
						path: 'personal-saved-vacancys',
						name: 'personal-saved-vacancys',
						component: () => import('../views/Personal-saved-vacancys.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
					{
						path: 'personal-mailing-list',
						name: 'personal-mailing-list',
						component: () => import('../views/Personal-mailing-list.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
					{
						path: 'personal-settings',
						name: 'personal-settings',
						component: () => import('../views/Personal-settings.vue'),
						meta: {
							innerPage: true,
							headerBackgroundImgPersonal: true
						},
					},
				]
			},
			{
				path: 'company-authenticate',
				name: 'company-authenticate',
				component: () => import('../views/Company-authenticate.vue'),
				meta: {
					innerPage: true,
					headerBackgroundImgPersonal: true
				},
			},
			{
				path: 'navoiy-district',
				name: 'navoiy-district',
				component: () => import('../views/Navoiy-district.vue'),
				meta: {
					simpleHeader: true
				}
			},
		]
	},
	{
		path: '/guide',
		name: 'guide',
		component: () => import('../views/Guide.vue'),
		meta: {
			frontMainTitle: true,
			frontSearchVacancyForm: true,
			headerBackgroundDefault: true
		}
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes,
	scrollBehavior () {
		return {x: 0, y: 0}
	}
})

export default router
